package Homework1

fun main() {
    myFun("12", "122", "1234", "fpo")
}

fun myFun(vararg params: String) {

    println("передано ${params.size} элемента/ов")
    println(params.toMutableList().joinToString(";", "", ";"))
}