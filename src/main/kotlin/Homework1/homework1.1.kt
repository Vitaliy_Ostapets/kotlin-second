package Homework1

import java.time.LocalDate

fun main(){
    val user1 = createUser("Валентин", "Николаев", "Отчествович", "X", dateOfBirth = (LocalDate.ofYearDay(2000, 10)), "11", "12313", "213132")
    val user2 = createUser("Валентин", "Николаев",  gender="X", dateOfBirth = (LocalDate.ofYearDay(2000, 10)), age = "22")
    val user3 = createUser("Валентин", "Николаев",  gender="X", dateOfBirth = (LocalDate.ofYearDay(2000, 10)), age = "11", snils = "123123")
}

fun createUser(name:String, surname:String, patronymic:String? = null, gender:String, dateOfBirth:LocalDate, age:String, inn:String? = null, snils:String?=null ){
    val returnPatronumic = patronymic?.let { ", patronymic = $patronymic" } ?:""
    val returnInn = inn?.let { ", inn = $inn" } ?:""
    val returnSnils = snils?.let { ", snils = $snils" } ?:""
    println("name = $name, surname = $surname$returnPatronumic, gender = $gender, age = $age, dateOfBirth = $dateOfBirth$returnInn$returnSnils")
}
