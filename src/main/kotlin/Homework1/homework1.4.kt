package Homework1

fun main() {
    CompanionClass.counter()
    CompanionClass.counter()
    CompanionClass.counter()

}
class CompanionClass {

    companion object {
        var counter: Int = 1

        fun counter() {
            println("Вызван counter. Количество вызовов = $counter")
            counter++
        }
    }
}