package Homework1

fun main() {
    val user1 = createUserNew("Петр", "Фамилиев", "Отчествович", "X", "25-11-2000", "11", "12313", "213132")
    val user2 = createUserNew("Петр", "Фамилиев",  gender="X", dateOfBirth = "25-11-2000", age = "11")
    val user3 = createUserNew("Петр", "Фамилиев",  gender="X", dateOfBirth = "25-11-2000", age = "11", snils = "123123")
    println(user1)
    println(user2)
    println(user3)
}

fun createUserNew(name:String, surname:String, patronymic:String? = null, gender:String, dateOfBirth:String, age:String, inn:String? = null, snils:String?=null)  =
    Person(name,surname,patronymic,gender,dateOfBirth,age,inn,snils)



data class Person(
    val name:String,
    val surname:String,
    val patronymic:String?,
    var gender:String,
    val dateOfBirth:String,
    val age:String,
    val inn:String?,
    val snils:String?
)