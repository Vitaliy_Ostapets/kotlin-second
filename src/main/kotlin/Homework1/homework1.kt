package Homework1

import java.util.Collections

fun main (){
    val list = mutableListOf(1, 4, 9, 16, 25)
    println(list.square())
    println(list.map { it*it })
}


fun MutableList<Int>.square(): MutableList<Int> {
    return this.map { it -> it*it } as MutableList<Int>
}

