package Homework1

import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate

fun main() {
    detectType("GADADG")
    detectType(2)
    detectType(6.1231)
    detectType(LocalDate.of(2014, 12, 6))
    detectType('a')
    detectType()
}

fun detectType(a: Any? = null) {
    when (a) {
        is String -> println("Я получил тип String = $a ее длина равна ${a.length} символ")
        is Int -> println("Я получил Int = $a, его квадра равен ${a.square()}")
        is Double -> println("Я получил Double = $a, это число округляется до ${a.round()}")
        is LocalDate -> println("Я получил LocalData = $a, эта дата ${a.tinkoffBirthDateCompare()} чем дата основания Tinkoff")
        else -> println("Я получил ${a?.javaClass}. Мне этот тип неизвестен или значение null(")
    }
}

fun Int.square() = this * this
fun Double.round() = BigDecimal(this).setScale(2, RoundingMode.HALF_EVEN)
fun LocalDate.tinkoffBirthDateCompare(): String =
    if (this < LocalDate.of(2006, 12, 24)) {
        "меньше"
    } else if (this == LocalDate.of(2006, 12, 24)) {
        "равно"
    } else "больше"